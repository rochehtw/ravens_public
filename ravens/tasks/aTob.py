# coding=utf-8
# Copyright 2021 The Ravens Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Insertion Tasks."""

import numpy as np
from ravens.tasks.task import Task
from ravens.utils import utils

import pybullet as p

"""
Task to add cube (urdf) and a target area (urdf)
"""
class AtoB(Task):
  """Insertion Task - Base Variant."""

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.max_steps = 3

  def reset(self, env):
    super().reset(env)
    block_id = self.add_source(env)
    targ_pose = self.add_target(env)
    # self.goals.append(
    #     ([block_id], [2 * np.pi], [[0]], [targ_pose], 'pose', None, 1.))
    self.goals.append(([(block_id, (2 * np.pi, None))], np.int32([[1]]),
                       [targ_pose], False, True, 'pose', None, 1))

  def add_source(self, env):
    """Add L-shaped block."""
    size = (0.1, 0.1, 0.04)
    urdf = 'block/block.urdf'
    pose = self.get_random_pose(env, size)
    #pose = ((0.328125, 0.39375000000000004, 0.02), (0.0, -0.0, 0.03188210227538523, -0.9994916365605576))
    return env.add_object(urdf, pose)

  def add_target(self, env):
    """Add L-shaped fixture to place block."""
    size = (0.1, 0.1, 0.0)
    urdf = 'zone/zone.urdf'
    #pose = self.get_random_pose(env, size)
    pose = ((0.328125, 0.39375000000000004, 0.0), (0.0, 0.0, 0.1, 0.0))
    env.add_object(urdf, pose, 'fixed')
    return pose

