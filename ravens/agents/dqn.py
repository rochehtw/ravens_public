# coding=utf-8
# Copyright 2021 The Ravens Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Dummy Agent."""

import os
import matplotlib.pyplot as plt

import numpy as np
from absl import flags

import torch
import torchvision.transforms as T
from PIL import Image
import random
import math
from ravens.models.DQN import DQN
from ravens.tasks import cameras
from ravens.tasks.task import Task
from ravens.tasks.aTob import AtoB
from ravens.utils import utils
from ravens.environments.environment import Environment
from ravens.memory.ReplayMemory import ReplayMemory

import tensorflow as tf
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
policy_net = DQN(device,40, 80, 6).to(device)
target_net = DQN(device,40, 80, 6).to(device)
target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

MAX_MEMORY = 100_000
BATCH_SIZE = 1000
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 200
steps_done = 0

LR = 0.001

class DQNAgent:
  """Dummy Agent."""

  def __init__(self,name):
    self.name = name
    self.total_iter = 0

    # Share same camera configuration as Transporter.
    self.camera_config = cameras.RealSenseD415.CONFIG

    # [Optional] Heightmap parameters.
    self.pixel_size = 0.003125
    self.bounds = np.float32([[0.25, 0.75], [-0.5, 0.5], [0, 0.28]])

    # A place to save pre-trained models.
    self.models_dir = os.path.join('checkpoints', self.name)
    if not tf.io.gfile.exists(self.models_dir):
      os.makedirs(self.models_dir)
 

  def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done)) # popleft if MAX_MEMORY is reached

  
  def preprocess(self, image):
    """Pre-process images (subtract mean, divide by std)."""
    color_mean = 0.18877631
    depth_mean = 0.00509261
    color_std = 0.07276466
    depth_std = 0.00903967
    image[:, :, :3] = (image[:, :, :3] / 255 - color_mean) / color_std
    image[:, :, 3:] = (image[:, :, 3:] - depth_mean) / depth_std
    return image

  # static pick position ((0.328125, 0.39375000000000004, 0.02), (0.0, -0.0, 0.03188210227538523, -0.9994916365605576))
  def select_action(self,state):
    # exploration and exploitation
    print(type(state))
    with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            #action = policy_net(state).max(1)[1].view(1, 1)

          #actions [x1,y1,z1,x2,y2,z2]  
          action = policy_net(state)
          list = action.flatten().tolist()
          res = {'pose0':((0.328125, 0.39375000000000004, 0.02), (0.0, -0.0, 0.03188210227538523, -0.9994916365605576)),'pose1':(list[3:6],[0,0,0,1])}
          #res = {'pose0':(list[0:3],[0,0,0,1]),'pose1':(list[4:6],[0,0,0,1])}
          print(res)
          return res
    


  def get_heightmap(self, obs, configs):
    """Reconstruct orthographic heightmaps with segmentation masks."""
    heightmaps, colormaps = utils.reconstruct_heightmaps(
        obs['color'], obs['depth'], configs, self.bounds, self.pixel_size)
    colormaps = np.float32(colormaps)
    heightmaps = np.float32(heightmaps)

    
    # Fuse maps from different views.
    valid = np.sum(colormaps, axis=3) > 0
    repeat = np.sum(valid, axis=0)
    repeat[repeat == 0] = 1
    colormap = np.sum(colormaps, axis=0) / repeat[Ellipsis, None]
    colormap = np.uint8(np.round(colormap))
    plt.figure()
    plt.imshow(colormap,
              interpolation='none')
    plt.title('Example extracted screen')
    plt.show()
    heightmap = np.sum(heightmaps, axis=0) / repeat


    
    colormap = colormap.transpose((2,0,1))
    colormap = np.ascontiguousarray(colormap, dtype=np.float32) / 255
    colormap = torch.from_numpy(colormap)
    print(np.shape(colormap))
    
    resize = T.Compose([T.ToPILImage(),
                    T.Resize(40, interpolation=Image.CUBIC),
                    T.ToTensor()])
    colormap = resize(colormap).unsqueeze(0)
    # Resize, and add a batch dimension (BCHW)
    print(type(colormap))
    #[B,C,H,W] Batch, Channel, Height, Weight
    print(np.shape(colormap))
    return colormap

def train():




    t = AtoB()
    agent = DQNAgent('dqn')
    env = Environment(
      '../environments/assets',      
      task=t,
      disp=True,
      shared_memory=False,
      hz=480)
    obs = env.reset()
    cmap = agent.get_heightmap(obs,cameras.RealSenseD415.CONFIG)
    
   

    memory = ReplayMemory(MAX_MEMORY)
    num_episodes = 50    
    

    while True:
        # select action based on picture state
        nextAction = agent.select_action(cmap)   
        #print(nextAction.numpy())     
        obs, reward, done, info = env.step(nextAction)
        env.reset()
        memory.push(obs,reward,done,info)
       
        

if __name__ == '__main__':
    train()
