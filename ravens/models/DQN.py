import torch
import torch.nn as nn
import torch.nn.functional as F

"""
# actions: {'pose0':([0.328125, 0.39375000000000004, 0.02],[0,0,0,1]),'pose1':(np.random.rand(3),[0,0,0,1])}
# actions number therfore is 6 p1(x,y,z) + p2(x,y,z)
class DQN(nn.Module):
    #actions 6 values (x,y,z,x2,y2,z2)
    def __init__(self,device,h,w,outputs) -> None:
        super(DQN, self).__init__()
        self.device = device
        self.conv1 = nn.Conv2d(3,16,kernel_size=5,stride=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16,32,kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32,32,kernel_size=5,stride=2)
        self.bn3 = nn.BatchNorm2d(32)

        def conv2d_size_out(size, kernel_size = 5, stride = 2):
            return (size - (kernel_size - 1) - 1) // stride  + 1

        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 32
        print(linear_input_size)
        self.head = nn.Linear(linear_input_size, outputs)

    
    #got (tuple, Parameter, Parameter, tuple, tuple, tuple, int)
    #expected (Tensor input, Tensor weight, Tensor bias, tuple of ints stride, tuple of ints padding, tuple of ints dilation, int groups)
    # here is were computation is done
   
    def forward(self, x):  
        x = x.to(self.device)
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))   
        x = self.head(x.view(x.size(0), -1))
        print(x)     
        return x

"""

class DQN(nn.Module):
    #actions: {'pose0':([0.328125, 0.39375000000000004, 0.02],[0,0,0,1]),'pose1':(np.random.rand(3),[0,0,0,1])}
    #actions 6 values (x,y,z,x2,y2,z2)
    def __init__(self,device,h,w,outputs) -> None:
        super(DQN, self).__init__()
        self.device = device
        self.conv1 = nn.Conv2d(3,16,kernel_size=5,stride=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16,32,kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32,32,kernel_size=5,stride=2)
        self.bn3 = nn.BatchNorm2d(32)
        self.adapt = nn.AdaptiveMaxPool2d((5,7))

    
        self.head = nn.Linear(32*5*7, outputs)

    
    #got (tuple, Parameter, Parameter, tuple, tuple, tuple, int)
    #expected (Tensor input, Tensor weight, Tensor bias, tuple of ints stride, tuple of ints padding, tuple of ints dilation, int groups)
    # here is were computation is done
   
    def forward(self, x):  
        x = x.to(self.device)
        x = F.max_pool2d(F.relu(self.conv1(x)),(5,2))
        x = self.adapt(F.relu(self.conv2(x)))
        x = x.view(-1, 32*5*7)
        x = self.head(x)
        return x
