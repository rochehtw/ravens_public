import random

from ravens.environments.environment import Environment
from ravens.utils import utils
from ravens.tasks import cameras
from ravens.tasks.aTob import AtoB
import matplotlib.pyplot as plt
import numpy as np

def run():
    # Add some objects(urdf) to the scene by using a task
    t = AtoB() 
    env = Environment(
      '../environments/assets',      
      task=t,
      disp=True,
      shared_memory=False,
      hz=480)
    
    #plt rgb + rgbd
    obs = env.reset()
    f,ax = plt.subplots(figsize=(20,10),ncols=2,nrows=1)
    ax = ax.flatten()
    ax[0].imshow(obs['color'][0])
    ax[1].imshow(obs['depth'][0])
    plt.show()
    
    # top down view
    bounds = np.array([[0.25, 0.75], [-0.5, 0.5], [0, 0.3]])
    cmap, hmap = utils.get_fused_heightmap(obs, cameras.RealSenseD415.CONFIG, bounds, 0.003)
    f,ax = plt.subplots(figsize=(10,10),ncols=2,nrows=1)
    ax[0].imshow(cmap)
    ax[0].set_title("RGB Image")
    ax[1].set_title("Height Map")
    ax[1].imshow(hmap)
    plt.show()

    
    for episode in range(5):
        env.reset()
        env.render()
        # example action

        coords_pick = tuple(random.random() for _ in range(3))
        coords_place = tuple(random.random() for _ in range(3))

        #robot_orientation = tuple(random.random() for _ in range(4))
        # for static tool orientation
        robot_orientation = (0,0,0,1)

        action = {'pose0':(coords_pick, robot_orientation),'pose1':((coords_place, robot_orientation))}
        (obs, reward, done, info) = env.step(action)

        #print some observation value
        print(reward, done, info)       


if __name__ == '__main__':
    run()