import pickle
from textwrap import wrap
import matplotlib.pyplot as plt
from ravens.utils import utils
from ravens.tasks import cameras
import numpy as np

color, depth, info = [] , [], []
folder = './dump/'
obs = {'color':[],'depth':[], 'info':[]}
features = ['color','depth','info']
def load_files(folder, features):
    for f in features:
        with open(folder+str(f)+"/0.pkl", 'rb') as pickle_file:
            obs[f].append(
                pickle.load(pickle_file)
            )
load_files(folder, features)



bounds = np.array([[0.25, 0.75], [-0.5, 0.5], [0, 0.3]])

#method from ravens for creating table top tranformations of the views
cmap, hmap = utils.get_fused_heightmap(obs, cameras.RealSenseD415.CONFIG, bounds, 0.003)
    


fig = plt.figure(figsize=(10, 15))
ax_color = fig.add_subplot(3,3,1)
ax_depth = fig.add_subplot(3,3,3)
ax_cmap = fig.add_subplot(3,3,4)
ax_hmap = fig.add_subplot(3,3,6)

ax_color.imshow(obs['color'][0])
ax_depth.imshow(obs['depth'][0])
ax_cmap.imshow(cmap)
ax_hmap.imshow(hmap)

fig.text(0.5,0.2,s=str(obs['info']), ha='center', wrap=True)
plt.show()


