import random

from ravens.environments.environment import Environment
from ravens.utils import utils
from ravens.tasks import cameras
from ravens.tasks.aTob import AtoB
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf
import pickle


'''
Simple generator to save rbg, rbgd + postions of target and source into pickle files



'''
def run():
    # Add some objects(urdf) to the scene by using a task
    t = AtoB() 
    env = Environment(
      '../environments/assets',      
      task=t,
      disp=True,
      shared_memory=False,
      hz=480)
    
    #plt rgb + rgbd
    obs = env.reset()
    f,ax = plt.subplots(figsize=(20,10),ncols=2,nrows=1)
    ax = ax.flatten()
    ax[0].imshow(obs['color'][0])
    ax[1].imshow(obs['depth'][0])
    plt.show()
    
    # top down view
    bounds = np.array([[0.25, 0.75], [-0.5, 0.5], [0, 0.3]])
    cmap, hmap = utils.get_fused_heightmap(obs, cameras.RealSenseD415.CONFIG, bounds, 0.003)
    f,ax = plt.subplots(figsize=(10,10),ncols=2,nrows=1)
    ax[0].imshow(cmap)
    ax[0].set_title("RGB Image")
    ax[1].set_title("Height Map")
    ax[1].imshow(hmap)
    plt.show()

    
    for episode in range(5):
      obs = env.reset()
      
        # save data
      
      add_single_observation(obs,env.info,episode)
      
         
def add_single_observation(obs,info, seed):
    """Add an episode to the dataset.

    Args:
      seed: random seed used to initialize the episode.
      episode: list of (obs, act, reward, info) tuples.
    """
    
    color = obs['color'][0]
    depth = obs['depth'][0]
    
  
    
    color = np.uint8(color)
    depth = np.float32(depth)

    def dump(data, field):
      field_path = os.path.join('./dump', field)
      if not tf.io.gfile.exists(field_path):
        tf.io.gfile.makedirs(field_path)
      fname = f'{seed}.pkl'  # -{len(episode):06d}
      with tf.io.gfile.GFile(os.path.join(field_path, fname), 'wb') as f:
        pickle.dump(data, f)

    dump(color, 'color')
    dump(depth, 'depth')
    dump(info, 'info')

if __name__ == '__main__':
    run()