# Ravens - Transporter Networks
forked from
**Team:** this repository is developed and maintained by [Andy Zeng](https://andyzeng.github.io/), [Pete Florence](http://www.peteflorence.com/), [Daniel Seita](https://people.eecs.berkeley.edu/~seita/), [Jonathan Tompson](https://jonathantompson.github.io/), and [Ayzaan Wahid](https://www.linkedin.com/in/ayzaan-wahid-21676148/). This is the reference repository for the paper:

### Transporter Networks: Rearranging the Visual World for Robotic Manipulation
[Project Website](https://transporternets.github.io/)&nbsp;&nbsp;•&nbsp;&nbsp;[PDF](https://arxiv.org/pdf/2010.14406.pdf)&nbsp;&nbsp;•&nbsp;&nbsp;Conference on Robot Learning (CoRL) 2020

## Installation

**Step 1.** Recommended: install [Miniconda](https://docs.conda.io/en/latest/miniconda.html) with Python 3.7.

```shell
curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b -u
echo $'\nexport PATH=~/miniconda3/bin:"${PATH}"\n' >> ~/.profile  # Add Conda to PATH.
source ~/.profile
conda init
```

**Step 2.** Create and activate Conda environment, then install GCC and Python packages.

```shell
cd ~/ravens
conda create --name ravens python=3.7 -y
conda activate ravens
sudo apt-get update
sudo apt-get -y install gcc libgl1-mesa-dev
pip install -r requirements.txt
python setup.py install --user
```

**Step 3.** Recommended: install GPU acceleration with NVIDIA [CUDA](https://developer.nvidia.com/cuda-toolkit) 10.1 and [cuDNN](https://developer.nvidia.com/cudnn) 7.6.5 for Tensorflow.
```shell
./oss_scripts/install_cuda.sh  #  For Ubuntu 16.04 and 18.04.
conda install cudatoolkit==10.1.243 -y
conda install cudnn==7.6.5 -y
```

### Alternative: Pure `pip`

As an example for Ubuntu 18.04:

```shell
./oss_scipts/install_cuda.sh  #  For Ubuntu 16.04 and 18.04.
sudo apt install gcc libgl1-mesa-dev python3.8-venv
python3.8 -m venv ./venv
source ./venv/bin/activate
pip install -U pip
pip install scikit-build
pip install -r ./requirements.txt
export PYTHONPATH=${PWD}
```

## Getting Started

### testing the environment

```python
def run():
    # Add some objects(urdf) to the scene by using a task
    t = AtoB() 
    env = Environment(
      '../environments/assets',      
      task=t,
      disp=True,
      shared_memory=False,
      hz=480)
    
    #init the scene
    env.reset()   

    
    for episode in range(5):
        env.render()
        # example action

        coords_pick = tuple(random.random() for _ in range(3))
        coords_place = tuple(random.random() for _ in range(3))

        #robot_orientation = tuple(random.random() for _ in range(4))
        # for static tool orientation
        robot_orientation = (0,0,0,1)

        action = {'pose0':(coords_pick, robot_orientation),'pose1':((coords_place, robot_orientation))}
        #obs['color'], obs['depth']
        (obs, reward, done, info) = env.step(action)

        #print some observation value
        #obs consists of rgbd data
        print(obs , reward, done, info)       


if __name__ == '__main__':
    run()

```

**Running the agent** 
```shell
cd agents
python dqn.py
```


**Defining models**  
Models should be defined here: 
[ravens/models](https://gitlab.com/-/ide/project/smartproductionsystems/projects/rltopics/ravens/tree/main/-/ravens/models/DQN.py/#L45)

example cnn model with pytorch

```python
class DQN(nn.Module):   
    def __init__(self,device,h,w,outputs) -> None:
        super(DQN, self).__init__()
        self.device = device
        self.conv1 = nn.Conv2d(3,16,kernel_size=5,stride=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16,32,kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32,32,kernel_size=5,stride=2)
        self.bn3 = nn.BatchNorm2d(32)
        self.adapt = nn.AdaptiveMaxPool2d((5,7))

    
        self.head = nn.Linear(32*5*7, outputs)
    
    # responsible for computation
    def forward(self, x):  
        x = x.to(self.device)
        x = F.max_pool2d(F.relu(self.conv1(x)),(5,2))
        x = self.adapt(F.relu(self.conv2(x)))
        x = x.view(-1, 32*5*7)
        x = self.head(x)
        return x
```



**tasks**  
To add objects to a scene use tasks
[ravens/tasks](https://gitlab.com/-/ide/project/smartproductionsystems/projects/rltopics/ravens/tree/main/-/ravens/tasks/aTob.py/)


## RL Structure

The MDP formulation for each task uses transitions with the following structure:

**Observations:** raw RGB-D images and camera parameters (pose and intrinsics).

**Actions:** a primitive function (to be called by the robot) and parameters.

**Rewards:** total sum of rewards for a successful episode should be =1.

**Info:** 6D poses, sizes, and colors of objects.
